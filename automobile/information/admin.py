from django.contrib import admin
from .models import Automobile, Manufacturer, Spares
# Register your models here.

admin.site.register(Automobile)
admin.site.register(Manufacturer)
admin.site.register(Spares)

# @admin.register(Automobile)
# class AutomobileAdmin(admin.ModelAdmin):
#     list_display = ("number_object", "title", "type_automobile", "mileage", "year")
#     readonly_fields = ("record_count",)


#     @admin.display(description="Автомобили")
#     def record_count(self, object):
#         return str(object.record_count)



