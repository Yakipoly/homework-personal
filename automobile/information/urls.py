from django.urls import path, include
from .views import AutomobileView, ManufacturerView, SparesView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register('automobile', AutomobileView, basename="automobile")
router.register('manufacturer', ManufacturerView, basename="manufacturer")
router.register('spares', SparesView, basename="spares")
urlpatterns = [
    path('information/', include(router.urls))
]

