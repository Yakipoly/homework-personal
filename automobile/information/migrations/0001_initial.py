# Generated by Django 3.2.14 on 2022-07-08 21:02

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Automobile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=256, verbose_name='Название')),
                ('mileage', models.IntegerField(verbose_name='Пробег')),
                ('year', models.PositiveSmallIntegerField(verbose_name='Год выпуска')),
            ],
        ),
    ]
