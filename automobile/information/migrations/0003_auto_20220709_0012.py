# Generated by Django 3.2.14 on 2022-07-08 21:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('information', '0002_auto_20220709_0003'),
    ]

    operations = [
        migrations.AlterField(
            model_name='automobile',
            name='mileage',
            field=models.PositiveIntegerField(verbose_name='Пробег '),
        ),
        migrations.AlterField(
            model_name='automobile',
            name='title',
            field=models.CharField(max_length=255, verbose_name='Название'),
        ),
    ]
