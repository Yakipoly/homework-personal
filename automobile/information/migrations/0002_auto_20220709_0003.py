# Generated by Django 3.2.14 on 2022-07-08 21:03

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('information', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.CharField(choices=[('RUS', 'Россия'), ('USA', 'США'), ('JPN', 'Япония'), ('CHN', 'Китай'), ('GER', 'Германия')], default='RUS', max_length=3, verbose_name='Страна производителя')),
            ],
            options={
                'verbose_name': 'Cтрана производителя',
            },
        ),
        migrations.AlterModelOptions(
            name='automobile',
            options={'verbose_name': 'Название автомобиля', 'verbose_name_plural': 'Названия автомобилей'},
        ),
        migrations.AddField(
            model_name='automobile',
            name='type_automobile',
            field=models.CharField(choices=[('HB', 'Хэтчбек'), ('RS', 'Роадстер'), ('SD', 'Седан'), ('SC', 'Суперкар'), ('PU', 'Пикап')], default='PU', max_length=2, verbose_name='Тип автомобиля'),
        ),
        migrations.CreateModel(
            name='Spares',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Наименование')),
                ('amount', models.IntegerField(verbose_name='Количество запчастей')),
                ('id_auto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='information.automobile', verbose_name='Номер автомобиля')),
                ('id_country', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='information.manufacturer', verbose_name='Код производителя')),
            ],
            options={
                'verbose_name': 'Запчасть',
                'verbose_name_plural': 'Запчасти',
            },
        ),
    ]
