from rest_framework import viewsets
from .models import *
from .serializers import *


class AutomobileView(viewsets.ModelViewSet):
    
    queryset = Automobile.objects.all()
    def get_serializer_class(self, *args, **kwargs):
        return AutomobileSerializer
   
class ManufacturerView(viewsets.ModelViewSet):
    queryset = Manufacturer.objects.all()
    def get_serializer_class(self, *args, **kwargs):
      
        return ManufacturerSerializer

class SparesView(viewsets.ModelViewSet):
    queryset = Spares.objects.all()
    def get_serializer_class(self, *args, **kwargs):
      
        return SparesSerializer
