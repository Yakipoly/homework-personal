
from rest_framework import serializers
from .models import Automobile, Manufacturer, Spares


class ManufacturerSerializer (serializers.HyperlinkedModelSerializer):

      class Meta:
        model = Manufacturer
        fields = ('id','country')


class AutomobileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Automobile
        fields = ('id', 'id_country', 'id_country_id', 'title', 'type_automobile', 'mileage', 'year')
   

class SparesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Spares
        fields = ('id', 'id_auto_id','id_auto', 'id_country_id', 'id_country','name', 'amount')


