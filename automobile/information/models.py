from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Авто". Сущности: Автомобиль, Запчасти, Страна производства


class Manufacturer(models.Model):
    country_names = [
        ('RUS', 'Россия'),
        ('USA', 'США'),
        ('JPN', 'Япония'),
        ('CHN', 'Китай'),
        ('GER', 'Германия'),
    ]
    country = models.CharField(
        verbose_name="Страна производителя", 
        max_length=3,
        choices=country_names,
        default='RUS',
    )


    def __str__(self):
        return self.country

    class Meta:
        verbose_name = "Cтрана производителя"


class Automobile(models.Model):
    
    id_country = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, null=True, related_name='id_country')
    TYPE_AUTO = [
        ('HB', 'Хэтчбек'),
        ('RS', 'Роадстер'),
        ('SD', 'Седан'),
        ('SC', 'Суперкар'),
        ('PU', 'Пикап'),
    ]
    type_automobile = models.CharField(
        verbose_name="Тип автомобиля", 
        max_length=2,
        choices=TYPE_AUTO,
        default='PU',
    )


    title = models.CharField(verbose_name="Название", max_length=255)
    mileage = models.PositiveIntegerField(verbose_name="Пробег ", null=True)
    year = models.PositiveSmallIntegerField(
        verbose_name="Год выпуска",
        validators=[
            MaxValueValidator(2022),
            MinValueValidator(1900)
        ],
        null=True
    )

    def __str__(self):
        return self.title

    def __unicode__(self):
        return '%s' % (self.title)

    class Meta:
        verbose_name = "Название автомобиля"
        verbose_name_plural = "Названия автомобилей"


class Spares(models.Model):
    id_auto = models.ForeignKey(Automobile, verbose_name = "Номер автомобиля", on_delete=models.CASCADE)
    id_country = models.ForeignKey(Manufacturer, verbose_name = "Код производителя", on_delete=models.PROTECT)
    name = models.CharField(verbose_name="Наименование", max_length=255)
    amount = models.PositiveIntegerField(verbose_name="Количество запчастей")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Запчасть"
        verbose_name_plural = "Запчасти"

